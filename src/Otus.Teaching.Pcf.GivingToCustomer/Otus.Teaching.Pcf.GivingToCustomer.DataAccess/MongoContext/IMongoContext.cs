﻿using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.MongoContext
{
    public interface IMongoContext<T>
    {
        public IMongoCollection<T> Collection { get; }
    }
}