﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.MongoContext;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoContext<T> _context;

        public MongoRepository(IMongoContext<T> context)
        {
            _context = context;
        }

        public async Task AddAsync(T entity)
        {
            await _context.Collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _context.Collection.FindOneAndDeleteAsync(t => t.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var cursor = await _context.Collection.FindAsync(new BsonDocument());
            return await cursor.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var cursor = await _context.Collection.FindAsync(t => t.Id == id);
            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await _context.Collection.FindAsync(predicate);
            return await cursor.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var cursor = await _context.Collection.FindAsync(t => ids.Contains(t.Id));
            return await cursor.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await _context.Collection.FindAsync(predicate);
            return await cursor.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var update = new BsonDocument("$set", entity.ToBsonDocument());
            await _context.Collection.FindOneAndUpdateAsync(t => t.Id == entity.Id, update);
        }

        public async Task UpdateManyAsync(IEnumerable<T> entities)
        {
            var updates = new List<WriteModel<T>>();
            var filterBuilder = Builders<T>.Filter;

            foreach (var entity in entities)
            {
                var filter = filterBuilder.Where(x => x.Id == entity.Id);
                updates.Add(new ReplaceOneModel<T>(filter, entity));
            }

            await _context.Collection.BulkWriteAsync(updates);
        }
    }
}
