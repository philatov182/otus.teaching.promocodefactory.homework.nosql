﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using PromoCode = Otus.Teaching.Pcf.GivingToCustomer.Core.Domain.Mongo.PromoCode;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request) {

            return new PromoCode
            {
                Id = request.PromoCodeId,
                PartnerId = request.PartnerId,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Parse(request.BeginDate),
                EndDate = DateTime.Parse(request.EndDate)
            };
        }
    }
}
