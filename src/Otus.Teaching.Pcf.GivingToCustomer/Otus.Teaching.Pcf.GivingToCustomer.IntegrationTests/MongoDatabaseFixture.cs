﻿using System;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.MongoContext;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class MongoDatabaseFixture<T> : IDisposable
    {
        private readonly MongoTestDbInitializer _mongoTestDbInitializer;

        public MongoDatabaseFixture()
        {
            var mongoDbSettings = new MongoDbSettings
            {
                Database = "promocode_factory_administration_mongodb",
                Host = "localhost",
                Port = 27017
            };
            var options = Options.Create(mongoDbSettings);

            DbContext = new MongoContext<T>(options);
            

            _mongoTestDbInitializer = new MongoTestDbInitializer(options);
            _mongoTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _mongoTestDbInitializer.CleanDb();
        }

        public IMongoContext<T> DbContext { get; }
    }
}