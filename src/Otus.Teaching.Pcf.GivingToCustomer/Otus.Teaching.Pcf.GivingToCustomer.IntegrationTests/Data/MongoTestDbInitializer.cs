﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.MongoContext;
using Customer = Otus.Teaching.Pcf.GivingToCustomer.Core.Domain.Mongo.Customer;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public class MongoTestDbInitializer
    {
        private readonly IOptions<MongoDbSettings> _promoCodeDatabaseSettings;

        public MongoTestDbInitializer(IOptions<MongoDbSettings> promoCodeDatabaseSettings)
        {
            _promoCodeDatabaseSettings = promoCodeDatabaseSettings;
        }

        public void InitializeDb()
        {
            var mongoClient = new MongoClient(
                _promoCodeDatabaseSettings.Value.ConnectionString);

            mongoClient.DropDatabase(_promoCodeDatabaseSettings.Value.Database);
            var mongoDatabase = mongoClient.GetDatabase(
                _promoCodeDatabaseSettings.Value.Database);

            var preferenceCollection = mongoDatabase.GetCollection<Preference>(nameof(Preference));
            preferenceCollection.InsertMany(TestDataFactory.Preferences);

            var customerCollection = mongoDatabase.GetCollection<Customer>(nameof(Customer));
            customerCollection.InsertMany(TestDataFactory.MongoCustomers);
        }

        public void CleanDb()
        {
            var mongoClient = new MongoClient(
                _promoCodeDatabaseSettings.Value.ConnectionString);

            mongoClient.DropDatabase(_promoCodeDatabaseSettings.Value.Database);
        }
    }
}